import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class ApiService {

  constructor(private http: HttpClient) {
  }

  /**
   * Configure headers of any request.
   * @return {Headers} - header object
   */
  private setHeaders(): HttpHeaders {
    const headersFields = {
      'Content-Type': 'application/json',
      Accept: 'application/json',
    };
    return new HttpHeaders(headersFields);
  }

  public get(path: string): Observable<any> {
    return this.http.get(`${path}`, {headers: this.setHeaders()});
  }
}
