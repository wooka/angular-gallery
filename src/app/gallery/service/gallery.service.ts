import {Injectable} from '@angular/core';
import {ApiService} from './api.service';
import {Observable} from 'rxjs/Observable';
import {Gallery} from '../models/gallery.model';
import 'rxjs/add/operator/map';
import {StorageService} from './storage.service';

@Injectable()
export class GalleryService {
  constructor(private apiService: ApiService, private storageService: StorageService) {
  }

  getGalleryData(url): Observable<Gallery[]> {
    return this.apiService.get(url).map(response => {
      return response.map((image, index) => {
        image.id = index;
        return image;
      }).filter(image => {
        return this.storageService.getDeletedImagesFromStorage().indexOf(image.id) === -1;
      });
    });
  }
}
