import {Injectable} from '@angular/core';

@Injectable()
export class StorageService {
  private LOCAL_STORAGE_KEY = 'g-deleted';

  constructor() {
    this.setDefaultStorage();
  }

  setDefaultStorage() {
    if (!localStorage.getItem(this.LOCAL_STORAGE_KEY)) {
      localStorage.setItem(this.LOCAL_STORAGE_KEY, JSON.stringify([]));
    }
  }

  getDeletedImagesFromStorage() {
    return JSON.parse(localStorage.getItem(this.LOCAL_STORAGE_KEY));
  }

  addDeletedImageToStorage(i) {
    let a = JSON.parse(localStorage.getItem(this.LOCAL_STORAGE_KEY));
    a.push(i);
    localStorage.setItem(this.LOCAL_STORAGE_KEY, JSON.stringify(a));
  }

}
