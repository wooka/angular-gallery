export class Pagination {
  activePage: number;
  totalPages: number;
  displayPages: Array<number>;
  displayRightDots: boolean;
  displayLeftDots: boolean;
}
