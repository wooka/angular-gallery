export class Gallery {
  id: number;
  title: string;
  url: string;
  date: Date;
}
