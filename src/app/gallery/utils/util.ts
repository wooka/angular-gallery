import {Pagination} from '../models/pagination.model';

export class Util {

  static makePagination(totalRecords: number, perPage: number, currentPage: number) {
    let displayedPages: Array<number> = [];
    let totalPages: number = totalRecords % perPage === 0 ? Math.floor(totalRecords / perPage) : Math.floor(totalRecords / perPage) + 1;

    let displayLeftDots = false;
    let displayRightDots = false;

    if (currentPage - 2 > 0) displayLeftDots = true;
    if (currentPage - 1 > 0) displayedPages.push(currentPage - 1);
    if (currentPage + 1 <= totalPages) displayedPages.push(currentPage + 1);
    if (currentPage + 2 <= totalPages) displayRightDots = true;

    displayedPages.push(currentPage);

    return <Pagination>{
      activePage: currentPage,
      totalPages: totalPages,
      displayPages: displayedPages.sort((a, b) => {
        return a - b;
      }),
      displayRightDots: displayRightDots,
      displayLeftDots: displayLeftDots
    };
  }
}
