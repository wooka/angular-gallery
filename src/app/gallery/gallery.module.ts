import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {GalleryComponent} from './component/gallery.component';
import {ApiService} from './service/api.service';
import {GalleryService} from './service/gallery.service';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {StorageService} from './service/storage.service';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [
    ApiService,
    GalleryService,
    StorageService
  ],
  declarations: [
    GalleryComponent
  ],
  exports: [GalleryComponent]
})
export class GalleryModule {
}
