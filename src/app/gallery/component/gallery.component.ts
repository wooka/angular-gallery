import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {GalleryService} from '../service/gallery.service';
import {Gallery} from '../models/gallery.model';
import {Pagination} from '../models/pagination.model';
import {FormControl, FormGroup} from '@angular/forms';
import {Subscription} from 'rxjs/Subscription';
import {Util} from '../utils/util';
import {StorageService} from '../service/storage.service';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.scss']
})
export class GalleryComponent implements OnInit, OnDestroy {
  @Input() auto: boolean;
  @Input() interval: number;

  private allImages: Array<Gallery>;
  private filteredImages: Array<Gallery>;
  public displayedImages: Array<Gallery>;

  public showModal = false;
  public modalImage: Gallery;
  public modalTimer;

  public pagination: Pagination;

  public galleryFilters: FormGroup;

  public displayPerPage = [5, 10, 15, 20];
  public sortBy = ['title', 'date'];
  public currentPage = 1;

  private galleryFiltersSub: Subscription;

  private DEFAULT_IMG_URL = 'https://s.yimg.com/pw/images/en-us/photo_unavailable.png';

  constructor(private galleryService: GalleryService, private storageService: StorageService) {
  }

  async ngOnInit() {
    this.galleryFilters = new FormGroup({
      perPage: new FormControl(),
      sortBy: new FormControl(),
      search: new FormControl()
    });

    this.galleryService.getGalleryData('./assets/data.json').subscribe(async response => {
      this.allImages = await Promise.all(response.map(async (image, index) => {
        if (!await this.checkImage(image.url)) image.url = this.DEFAULT_IMG_URL;
        return image;
      }));

      this.onFilterChanges();
      this.galleryFilters.get('perPage').setValue(10);
      this.galleryFilters.get('sortBy').setValue('title');
    });
  }

  ngOnDestroy() {
    this.galleryFiltersSub.unsubscribe();
    this.clearTimer();
  }

  private onFilterChanges() {
    this.galleryFiltersSub = this.galleryFilters.valueChanges.subscribe((filter) => {
      this.filteredImages = this.allImages.sort((a, b) => {
        switch (filter.sortBy) {
          case 'title':
            if (a[filter.sortBy].toLowerCase() > b[filter.sortBy].toLowerCase()) return 1;
            if (a[filter.sortBy].toLowerCase() < b[filter.sortBy].toLowerCase()) return -1;
            return 0;
          case 'date':
            return new Date(b[filter.sortBy]).getTime() - new Date(a[filter.sortBy]).getTime();
        }
      });

      if (filter.search) {
        this.filteredImages = this.filteredImages.filter(image => {
          return (image.title.toLowerCase()).indexOf(filter.search.toLowerCase()) >= 0;
        });
      }

      this.showPage(1);
    });
  }

  private showPage(page = 1) {
    this.currentPage = page;
    let perPage = this.galleryFilters.get('perPage').value;
    let offset = (this.currentPage - 1) * perPage;

    this.pagination = Util.makePagination(this.filteredImages.length, perPage, this.currentPage);
    this.displayedImages = this.filteredImages.slice(offset, offset + perPage);
  }

  public initTimer() {
    if (this.auto) {
      this.modalTimer = setInterval(() => {
        this.nextSlide();
      }, this.interval * 1000);
    }
  }

  public clearTimer() {
    if (this.auto) {
      clearInterval(this.modalTimer);
    }
  }

  public removeItem(image, event) {
    event.stopPropagation();
    this.storageService.addDeletedImageToStorage(image.id);

    this.allImages.splice(this.allImages.indexOf(image), 1);
    this.filteredImages.splice(this.filteredImages.indexOf(image), 1);

    this.showPage(this.currentPage);
  }

  public changePage(page) {
    this.showPage(page);
  }

  private checkImage(url) {
    return new Promise((resolve, reject) => {
      let img = new Image();
      img.onload = () => resolve(true);
      img.onerror = () => resolve(false);
      img.src = url;
    });
  }

  public displayModal(image) {
    this.showModal = true;
    this.modalImage = image;

    this.initTimer();
  }

  public closeModal() {
    this.showModal = false;
    this.clearTimer();
  }

  public prevSlide(reInitTimer = false) {
    this.changeSlide('left', reInitTimer);
  }

  public nextSlide(reInitTimer = false) {
    this.changeSlide('right', reInitTimer);
  }

  private changeSlide(type, reInitTimer) {
    if (reInitTimer) {
      this.clearTimer();
      this.initTimer();
    }

    let indexOfCurrentModalImage = this.filteredImages.indexOf(this.modalImage);
    let indexNextSlide: number;
    switch (type) {
      case 'left':
        indexNextSlide = indexOfCurrentModalImage === 0 ? this.filteredImages.length - 1 : indexOfCurrentModalImage - 1;
        break;
      case 'right':
      default:
        indexNextSlide = indexOfCurrentModalImage === this.filteredImages.length - 1 ? 0 : indexOfCurrentModalImage + 1;
        break;
    }
    this.modalImage = this.filteredImages[indexNextSlide];
  }
}
